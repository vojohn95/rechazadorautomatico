package main

import (
	"database/sql"
	"fmt"
	"goservices/config"
	"goservices/middlewares"
	"goservices/pkg/utils"
	"log"
	"os"
	"strconv"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql" // import the mysql driver package
	"github.com/schollz/progressbar/v3"
	"gopkg.in/gomail.v2"

	"github.com/adhocore/chin"
	"github.com/fatih/color"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-module/carbon/v2"
)

type AutofTicket struct {
	ID            uint           `gorm:"column:id"`
	ClienteID     uint           `gorm:"column:id_cliente"`
	EstID         uint           `gorm:"column:id_est"`
	TipoID        uint           `gorm:"column:id_tipo"`
	CentralUserID uint           `gorm:"column:id_CentralUser"`
	Factura       string         `gorm:"column:factura"`
	NoTicket      string         `gorm:"column:no_ticket"`
	TotalTicket   float64        `gorm:"column:total_ticket"`
	Estatus       string         `gorm:"column:estatus"`
	UsoCFDI       string         `gorm:"column:UsoCFDI"`
	MetodoPago    string         `gorm:"column:metodo_pago"`
	FormaPago     string         `gorm:"column:forma_pago"`
	RFC           string         `gorm:"column:RFC"`
	RazonSocial   string         `gorm:"column:Razon_social"`
	Email         string         `gorm:"column:email"`
	Park          sql.NullString `gorm:"column:park"`
	CP            sql.NullString `gorm:"column:cp"`
	RegimenID     *uint          `gorm:"column:id_regimen"`
	CodigoRegimen sql.NullString `gorm:"column:codigoRegimen"`
	Descripcion   sql.NullString `gorm:"column:descripcion"`
	CreatedAt     string         `gorm:"column:created_at"`
}

func main() {
	carbon.Now().ToDateTimeString()
	// Define Fiber config.
	config := config.FiberConfig()

	//start fiber
	app := fiber.New(config)
	// Middlewares.
	middlewares.Params(app)

	//connect database
	//databases.Connect()

	var wg sync.WaitGroup

	s := chin.New().WithWait(&wg)
	go s.Start()

	go longTaskRechazos(&wg)

	s.Stop()
	wg.Wait()

	// Start server (with or without graceful shutdown).
	if os.Getenv("STAGE_STATUS") == "dev" {
		utils.StartServer(app)
	} else {
		utils.StartServerWithGracefulShutdown(app)
	}

}

func longTaskRechazos(wg *sync.WaitGroup) {
	delay := time.Second * 6
	time.Sleep(delay)
	// Open the log file in append mode and set it as the default log output
	logFile, err := os.OpenFile("logs/result.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatal(err)
	}
	defer logFile.Close()
	log.SetOutput(logFile)

	// Write a log message
	color.Red("Empieza envio de correo!")
	log.Println("Empieza envio de correo!")

	//query
	// Call the conexion function to connect to the database
	db, err := conexion()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	// Query the autof_tickets table
	rows, err := db.Query("select `autof_tickets`.`id`, `autof_tickets`.`id_cliente`, `autof_tickets`.`id_est`, `autof_tickets`.`id_tipo`, `autof_tickets`.`id_CentralUser`, `autof_tickets`.`factura`, `autof_tickets`.`no_ticket`, `autof_tickets`.`total_ticket`, `autof_tickets`.`estatus`, `autof_tickets`.`UsoCFDI`, `autof_tickets`.`metodo_pago`, `autof_tickets`.`forma_pago`, `autof_tickets`.`RFC`, `autof_tickets`.`Razon_social`, `autof_tickets`.`email`, `parks`.`nombre` as `park`, `autof_tickets`.`cp`, `autof_tickets`.`id_regimen`, `catalogo_regimen`.`codigoRegimen`, `catalogo_regimen`.`descripcion`, `autof_tickets`.`created_at` from `autof_tickets` inner join `parks` on `parks`.`no_est` = `autof_tickets`.`id_est` left join `catalogo_regimen` on `catalogo_regimen`.`codigoRegimen` = `autof_tickets`.`id_regimen` where (`estatus` = 'validar' or `estatus` = 'sin_fact' and `autof_tickets`.`id_regimen` is null or `autof_tickets`.`id_regimen` = '' and year(`autof_tickets`.`created_at`) >= 2023) and `autof_tickets`.`deleted_at` is null order by `id` desc")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	// Iterate over the rows and scan them into AutofTicket structs
	var tickets []AutofTicket
	for rows.Next() {
		var ticket AutofTicket
		err := rows.Scan(
			&ticket.ID,
			&ticket.ClienteID,
			&ticket.EstID,
			&ticket.TipoID,
			&ticket.CentralUserID,
			&ticket.Factura,
			&ticket.NoTicket,
			&ticket.TotalTicket,
			&ticket.Estatus,
			&ticket.UsoCFDI,
			&ticket.MetodoPago,
			&ticket.FormaPago,
			&ticket.RFC,
			&ticket.RazonSocial,
			&ticket.Email,
			&ticket.Park,
			&ticket.CP,
			&ticket.RegimenID,
			&ticket.CodigoRegimen,
			&ticket.Descripcion,
			&ticket.CreatedAt,
		)
		if err != nil {
			log.Fatal(err)
		}
		tickets = append(tickets, ticket)
	}

	// Prepare the update statement
	stmt, err := db.Prepare("UPDATE autof_tickets SET estatus = ?, coment = ? WHERE id = ?")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()

	color.Yellow("Tickets a rechazar: %d", len(tickets))
	log.Printf("Tickets a rechazar: %v\n", len(tickets))

	bar := progressbar.Default(int64(len(tickets)))

	i := 0
	// Iterate over the tickets and update the status
	for _, ticket := range tickets {
		bar.Add(1)
		// Execute the update statement with the ticket ID and status "Rechazo"
		_, err := stmt.Exec("Rechazo", "Se solicita reingrese su solicitud dentro del portal de facturación", ticket.ID)
		if err != nil {
			log.Fatal(err)
		}

		message := "<html><h4 style='color: green;' align='center;'>Operadora Central de Estacionamientos SAPI S.A. de C.V.</h4>" +
			"<p align='center;'>Esta rechazando su factura por: Se solicita reingrese su solicitud dentro del portal de facturación </p>" +
			"<p align='center;'>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</p>" +
			"<p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico " + strconv.Itoa(int(ticket.ID)) + " a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p></html>"

		m := gomail.NewMessage()
		m.SetHeader("From", "facturacion-oce@central-mx.com")
		m.SetHeader("To", ticket.Email)
		m.SetHeader("Subject", "Proceso Rechazo Factura automatico")
		m.SetBody("text/html", message)

		d := gomail.NewDialer("smtp.office365.com", 587, "facturacion-oce@central-mx.com", "1t3gr4d0r2020*")

		// Send the email
		if err := d.DialAndSend(m); err != nil {
			fmt.Printf("Error al enviar correo:   ")
			fmt.Println(err)
			log.Printf("Error al enviar correo: %v", err)
		} else {
			//color.Green(fmt.Sprintf("%v - Email sent successfully!", ticket.ID))
			log.Printf("Email sent successfully: %d\n", ticket.ID)
		}

		i++
	}

	message := "<html><h4 style='color: green;' align='center;'>Operadora Central de Estacionamientos SAPI S.A. de C.V.</h4>" +
		"<p align='center;'>Se ha realizado el proceso automatico de rechazos y envio de correos </p>" +
		"<p align='center;'>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</p>" +
		"<p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p></html>"

	m := gomail.NewMessage()
	m.SetHeader("From", "facturacion-oce@central-mx.com")
	m.SetHeader("To", "lfernando@integrador-technology.mx")
	m.SetHeader("Subject", "Rechazo Factura")
	m.SetBody("text/html", message)

	d := gomail.NewDialer("smtp.office365.com", 587, "facturacion-oce@central-mx.com", "1t3gr4d0r2020*")

	// Send the email
	if err := d.DialAndSend(m); err != nil {
		fmt.Printf("Error al enviar correo:   ")
		fmt.Println(err)
	} else {
		color.Red("Email sent successfully to all!")
	}
}

func conexion() (*sql.DB, error) {
	dbUser := "devengers"
	dbPass := "C3Ntr4L%4PL1c4T1V0"
	dbHost := "201.161.84.233"
	dbPort := "3306"
	dbName := "central_aplicativo"

	dataSourceName := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", dbUser, dbPass, dbHost, dbPort, dbName)

	db, err := sql.Open("mysql", dataSourceName)

	if err != nil {
		return nil, err
	}

	// Set connection properties
	db.SetConnMaxLifetime(0)
	db.SetMaxIdleConns(0)
	db.SetMaxOpenConns(0)

	// Test the connection
	if err := db.Ping(); err != nil {
		return nil, err
	}

	color.Blue("Connected to database %s successfully\n", dbName)

	return db, nil
}
